import java.util.Iterator;
import java.util.List;

public class PreexistCondition implements RulesByHabits
{
	float percentage = 0.0f;
	
	public PreexistCondition(List<String> habits)
	{
		Iterator<String> itr = habits.iterator();
		int i = 1;
		while (itr.hasNext())
		{
			percentage = 0.01f * i;
			itr.next();
			i++;
		}
	}

	public float getPercentage() {
		
		return percentage;
		
	}

}
