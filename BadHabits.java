import java.util.Iterator;
import java.util.List;

public class BadHabits implements RulesByHabits {
	
	float percentage = 0.00f;
	
	public BadHabits(List<String> habits)
	{
		Iterator<String> itr = habits.iterator();
		int i = 1;
		while (itr.hasNext())
		{
			percentage = 0.03f * i;
			itr.next();
			i++;
		}
	}

	@Override
	public float getPercentage() {
		
		return percentage;
	}

}
