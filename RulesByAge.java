public class RulesByAge
{
	public float getPercentage(int age, String gender)
	{
		float returnPercentage = 0.0f;
		int additionalPercentage = 0;
		
		if (gender.equalsIgnoreCase("male"))
		{
			additionalPercentage = 2;
		}
		if ((age >= 18) && (age < 25))
			returnPercentage = (float)(additionalPercentage+10)/100;
		else if ((age >= 25) && (age < 30))
			returnPercentage = (float)(additionalPercentage+20)/100;
		else if ((age >= 30) && (age < 35))
		{
			returnPercentage = (float)(additionalPercentage+30)/100;
		}
		else if ((age >= 35) && (age < 40))
			returnPercentage = (float)(additionalPercentage+40)/100;
		else if (age >=40)
			returnPercentage = (float)(additionalPercentage+60)/100;
		
		return returnPercentage;
	}
	
	public int getBaseAmount()
	{
		return 5000;
	}
}
