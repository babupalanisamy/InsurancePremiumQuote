import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class QuoteUnitTest {
{
	public void testGoodHabitPercentage()
	{
		List<String> goodHabits = new ArrayList<String>();
		goodHabits.add("Exercise");
		RulesByHabits rbh = new GoodHabits(goodHabits);
		float percentage = rbh.getPercentage();
		assertEquals(0.03, percentage, 0.0);
	}

	public void testbadHabitPercentage()
	{
		List<String> badHabits = new ArrayList<String>();
		badHabits.add("Alcohol");
		RulesByHabits rbh = new BadHabits(badHabits);
		float percentage = rbh.getPercentage();
		assertEquals(0.03, percentage, 0.0);
	}

	public void testCurrentConditionPercentage()
	{
		List<String> currentCond = new ArrayList<String>();
		currentCond.add("Overweight");
		RulesByHabits rbh = new PreexistCondition(currentCond);
		float percentage = rbh.getPercentage();
		assertEquals(0.01, percentage, 0.0);
	}
	
	
}