import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PremiumQuoteGenerator {
	
	public static void main(String args[]){
		System.out.println("output ----------");
		RulesByAge percentageByAge = new RulesByAge();
		
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the name :");
		String name = s.nextLine();
		
		System.out.println("Enter the age :");
		int age = s.nextInt();
		
		System.out.println("Enter the gender :");
		String gender = s.next();
		
		float perByAge = percentageByAge.getPercentage(age, gender.trim());
		List<String> goodhabitsList = new ArrayList<String>();
		List<String> badhabitsList = new ArrayList<String>();
		List<String> currentHealth = new ArrayList<String>();
		
		getCurrentHealth(s, currentHealth);
		getHabits(s, goodhabitsList, badhabitsList);
		s.close();
	
		float baseamount = percentageByAge.getBaseAmount();
		PremiumQuoteGenerator pqg = new PremiumQuoteGenerator();
		float totalPremiumAmount = pqg.getPremiumAmount(goodhabitsList, badhabitsList, currentHealth, perByAge, baseamount);
		
		
		System.out.println("\n");
		System.out.println("Total Premium Amount = "+totalPremiumAmount);
	}
	
	private static void getCurrentHealth(Scanner s, List<String> currentHealth)
	{
		System.out.println("Current Health\nHypertension y/n:");
		String ht = s.next();
		if (ht.equalsIgnoreCase("y"))
			currentHealth.add("Hypertension");
		System.out.println("Blood Pressure y/n:");
		String bp = s.next();
		if (bp.equalsIgnoreCase("y"))
			currentHealth.add("Blood Pressure");
		System.out.println("Blood Sugar y/n: ");
		String bs = s.next();
		if (bs.equalsIgnoreCase("y"))
			currentHealth.add("Blood Sugar");
		System.out.println("Overweight y/n:");
		String ow = s.next();
		if (ow.equalsIgnoreCase("y"))
			currentHealth.add("Overweight");

	}
	
	private static void getHabits(Scanner s, List<String> goodhabitsList, List<String> badhabitsList)
	{
		System.out.println("Habits\nSmoking y/n:");
		String sm = s.next();
		if (sm.equalsIgnoreCase("y"))
			badhabitsList.add("Smoking");
		System.out.println("Alcohol y/n:");
		String al = s.next();
		if (al.equalsIgnoreCase("y"))
			badhabitsList.add("Alcohol");
		System.out.println("DailyExcersie y/n:");
		String de = s.next();
		if (de.equalsIgnoreCase("y"))
			goodhabitsList.add("DailyExcersie");
				
		System.out.println("Drug y/n:");
		String dr = s.next();
		if (dr.equalsIgnoreCase("y"))
			badhabitsList.add("Alcohol");
	}
	
	private float getPremiumAmount(List<String> goodhabitsList, List<String> badhabitsList, 
									List<String> currentHealth, float perByAge, float baseamount)
	{
		RulesByHabits perByGudHabits = new GoodHabits(goodhabitsList);
		RulesByHabits perByBadHabits = new BadHabits(badhabitsList);
		RulesByHabits perByCurrentHealth = new PreexistCondition(currentHealth);
		
		
		float totalPercentByCategory = perByGudHabits.getPercentage() + 
										perByBadHabits.getPercentage() + 
										perByCurrentHealth.getPercentage() +
										perByAge;
		
		
		float amtByCategoryPer = baseamount * totalPercentByCategory;
		
		// Calculate total premium amount
		float totalPremiumAmount = baseamount + amtByCategoryPer;
		return totalPremiumAmount;
		
	}

}
